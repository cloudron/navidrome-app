FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

# renovate: datasource=github-releases depName=navidrome/navidrome versioning=semver extractVersion=^v(?<version>.+)$
ARG NAVIDROME_VERSION=0.54.5

RUN curl -L https://github.com/navidrome/navidrome/releases/download/v${NAVIDROME_VERSION}/navidrome_${NAVIDROME_VERSION}_linux_amd64.tar.gz | tar zxvf - && \
    chmod +x /app/code/navidrome

COPY config.toml.template start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

