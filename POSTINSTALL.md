On first visit, you will be asked to set up an admin account.

By default, the music library is located at `/app/data/library`.

**IMPORTANT**: Passwords are stored in [clear text](https://github.com/deluan/navidrome/issues/202)
in the database for subsonic compatiblity. For this reason, we recommend using a random password.
