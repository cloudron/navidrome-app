#!/bin/bash

set -eu

mkdir -p /app/data/data /app/data/library

[[ ! -f /app/data/config.toml ]] && cp /app/pkg/config.toml.template /app/data/config.toml

chown -R cloudron:cloudron /app/data

# https://www.navidrome.org/docs/usage/configuration-options/
echo "==> Starting navidrome"
exec gosu cloudron:cloudron /app/code/navidrome --configfile /app/data/config.toml

